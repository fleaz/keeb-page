<!--
.. title: Impressum
.. slug: impressum
.. date: 2022-01-09 13:49:23 UTC+01:00
.. tags: 
.. category: 
.. type: text
-->

Niko Wenselowski<br />
Engeldamm 18<br />
10179 Berlin<br />
*keebs [at] nerdno . de*


## Datenschutz

Der Webserver, der diese Seite ausliefert, speichert die Seiten-Zugriffe der letzten 30 Tage in Form gekürzter IP-Adressen.
Dabei werden neben der gekürzten IP-Adresse der Zeitpunkt, die aufgerufene Datei inkl. Größe und der anfragende User-Agent gespeichert.
Im Fehlerfall werden diese Daten zur Analyse verwendet.

