<!--
.. title: Glossar
.. slug: glossar
.. date: 2022-01-12 20:57:00 UTC+02:00
.. tags:
.. category:
.. link:
.. description: Begriffe aus der Welt mechanischer Tastaturen einfach erklärt.
.. type: text
-->

Was bedeutet eigentlich...

## <a name="alphas">Alphas</a>
Mit **Alphas** bezeichnet man bei [Keycaps](#keycap) die Gruppe der Tasten in der Mitte der Tastatur die primär die
Buchstaben enthalten sowie am rechten Rand je nach [Layout](#keymap) noch Umlaute oder Sonderzeichen.

## <a name="clicky-switch">Clicky Switches</a>
Bei **taktilen Switches** handelt es sich um eine der drei Grundarten von Switches (neben [linear](#linear-switch) und
[taktil](#tactile-switch)). Bei clicky Switches hat man wie bei taktilen Switches auch einen Widerstand in der Mitte des
Tastendrucks der überwundern werden muss beim herunterdrücken, jedoch ist dieser nichtnur haptisch sondern auch deutlich
hörbar. Hierfür wird je nach Switch entweder klassischerweiße ein *click jacket* (bekannt aus Cherry MX Blue) verwendet
was ein kleiner Plastikrahmen um den Stem ist der durch eine Feder auf den Boden des [Housings](#housing) geschossen
wird, oder neuerdings eine *click bar* (bspw. Kailh BOX) was ein kleiner Metallstreifen ist welcher beim herunterdrücken
des Stems angeschnickt wird.

## <a name="dailydriver">Daily Driver</a>
Hiermit ist das Keyboard gemeint dass man im täglichen Einsatz hat bzw. am meisten benutzt.

## <a name="endgame">Endgame Keyboard</a>
Mit **Endgame** spricht man meistens über sein absolutes Wunschkeyboard was man gerne hätte.

## <a name="groupbuy">Firmware</a>
Eine **Firmware** wandelt einen Tastendruck in vom Computer verwertbare Informationen um.  Eine Firmware kann für
weitere Dinge, bspw. Leuchteffekt, zuständig sein.

Es gibt eine Vielzahl unterschiedlicher [Firmware](/firmware/).

## <a name="frankenswitch">Frankenswitches</a>
Bei einem **Frankenswitch** zerlegt man mehrere verschiedene Typen von Switches und kombiniert diesen zu einem neuen. Da
fast alle gängigen Switche auf dem Markt "MX Style" sind, lässt sich eigentlich fast alles miteinander kombinieren. Ein
Frankenswitch kann entweder rein ästhetische Gründe haben, man kann jedoch durch austauschen von [Stem](#stem) und
[Spring](#spring) auch die grundlegenden mechanischen Eigenschaften oder sein Soundprofil verändern und einen gänzlich
neuen Switch kreiren. Ein Beispiel hierfür wäre der "Ergoclear": Der MX Clear hat einen sehr beliebten
[taktilen](#tactile-switch) Bump, jedoch ist er den meisten Menschen die Feder deutlich zu hart weswegen sie die Feder
aus bspw. einem MX Brown nehmen und so die schöne taktilität des MX Clear mit einer schwächeren Feder kombiniern um ihn
angenehmer tippbar zu machen.

## <a name="groupbuy">Group Buy (GB)</a>
Mit **Group Buy** bezeichnet man die begrenzte Möglichkeit die Herstellung eines Produktes zu unterstützen.

Der [Ablauf einen Group Buys](/gb-process/) umfasst unterschiedliche Phasen, oftmals steht am Anfang ein [Interest
Check](#interestcheck).

## <a name="housing">Housing</a>
Das Gehäuse eines [Switches](#switch) wird als **Housing** bezeichnet und besteht aus einer Ober- und einer Unterseite
die als **Top housing** und **Bottom housing** bezeichnet werden.

## <a name="interestcheck">Interest Check (IC)</a>
Bei einem **Interest Check** wird das Interesse an einer Produktidee geprüft.

Einem erfolgreichen _IC_ folgt in der Regel ein [Group Buy](#groupbuy).


## <a name="keycap">Keycap</a>
Mit **Keycap** bezeichnet man die Kappe, welche auf einem [Switch](#switch) steckt.  Keycaps sind in der Regel aus
Plastik. Keycaps haben unterschiedliche [Profile](#profil).


## <a name="keymap">Keymap</a>
Eine **Keymap** steuert bei welchem Tastendruck welches Zeichen vom Computer interpretiert wird.  Diese ist unabhängig
von der [physikalischen Tastenanordnung](#layout) und von der Beschriftung der [Keycap](#keycap).

## <a name="layer">Layer</a>
Mit **Layer** bezeichnet man die verschiedenen Funktionen die ein Switch haben kann je nach dem welchen
[Modifier](#modifier) man drückt. Der erste Layer wäre zum Beispiel das Verhalten das die Taste mit dem "e" beim drücken
ein kleines "e" produziert. Drückt man jetzt zusätzlich noch Shift landet man in einem anderen Layer und die gleiche
Taste erzeugt ein großes E. Drückt man jetzt AltGr zusammen mit der Taste erscheint ein €. Wenn man jetzt auf seinem
Keyboard [QMK](#qmk) installiert hat, könnte man noch viele weiter Funktionen auf jede Beliebige Taste legen.  Durch
diese vielen Layer und das mehrfachbelegen von Tasten ist es möglich auf [kleinen Tastaturen](#groessen) zu tippen indem
man die benötigten Tasten einfach auf verschiedene Layer auslagert und so Platz spart.


## <a name="layout">Layout</a>
Mit **Layout** bezeichnet man in der Regel zwei Dinge:

1. Die physikalische Tastenanordnung auf einem Keyboard. Gängige Layouts sind [staggered](#staggered-layout) und
[ortholinear](#ortholinear-layout).
2. Die softwareseitige Tastenzuordnung, die [Keymap](#keymap).

## <a name="linear-switch">Lineare Switches</a>
Bei **linearen Switches** handelt es sich um eine der drei Grundarten von Switches (neben [taktil](#tactile-switch) und
[clicky](#clicky-switch)). Hier ändert sich die benötigte Kraft beim herunterdrücken nicht sonder bleibt _linear_ aber
man hat auch kein Feedback an welchem  Punkt der Switch auf seinem Weg nach unten ausgelöst hat und der Tastendruck
registriert wurde.

## <a name="lubing">Lubing</a>
Beim **Lubing** zerlegt man jeden einzelnen Switch in seine Bestandteile und trägt eine kleine Menge Schmiermittel
(bspw. Krytox) auf alle Teile auf die sich berühren und verringert damit die Reibung um ein noch weicheres Tippgefühl
und einen anderen Sound zu erzeugen. Kombiniert wird das Lubing meist mit dem Montieren von [Switch Films](#films) da
der Switch ja eh schon offen ist und so beides in einem Arbeitsgang erledigt werden kann.

## <a name="macropad">Macropad</a>
Ein **Macropad** ist im Prinzip eine eigene sehr kleine Tastatur auf der man Macrotasten, Mediakeys und sonstige
Spezialtasten kann. Macropads bewegen sich meist zwischen 4 und 16 Tasten und haben manchmal auchnoch Drehencoder mit
dem beispielsweise die Lautstärke geregelt werden kann.

## <a name="moq">Minimum Order Quantity (MOQ)</a>
**MOQ** ist die Abkürzung für _Minimum Order Quantity_, also die Mindestbestellmenge. **MOQs** werden meist als Teil
eines [Group Buys](#groupbuy) von Herstellern vorgegeben, ohne deren Erreichen sich die Herstellung nicht wirtschaftlich
oder nicht zum vereinbarten Preis lohnt.

## <a name="modifier">Modifier Tasten</a>
**Modifier** sind wie der Name schon sagt Tasten, welche das Verhalten von anderen Tasten beeinflussen. Das wohl
bekannteste Beispiel hierfür wäre die Shift-Taste die dafür sorgt dass während man sie drückt Großbuchstaben schreibt.
Modifier können entweder einen Effekt haben während man sie drückt (bspw: Shift) oder einen Zustand ein- und ausschalten
(bspw: Capslock).

Wenn man über [Keycap](#keycap) Sets spricht, bezeichnet man mit dem Begriff **Modifier** meist jedoch alle Tasten die
sich rund um die Buchstaben ([Alphas](#alphas)) ansiedeln. Also auch zum  Beispiel Strg,Alt,Enter und Backspace.

## <a name="ortholinear-layout">Ortholineares Layout</a>
Bei einem **ortholinearen Layout** sind die Tasten vertikal geradlinig angeordnet.  Manche Tastaturen haben darüber
hinaus eine horizontal geradlinige Anordnung, so dass sich ein Gitter ergibt.

Tastaturen mit einer ortholinearen Anordnung werden auch als _Orthos_ bezeichnet.

Das Gegenteil dieses Layouts ist das [staggered Layout](#staggered-layout).

## <a name="pcb">PCB</a>
Das **PCB** oder auf deutsch Platine ist das tragende Element in der Mitte jeder tastatur das alle elektrischen Bauteile
miterinander verbindet. Auf ihr befindet sich der Controller und der USB Port sowie alle Leiterbahnen die die Switche
miterinander verbinden und ein elektrisches Signal herstellen.

## <a name="pcb-mount">PCB mount</a>
Bezeichnet das Verfahren wenn Switches und/oder Stabilizer direkt auf das PCB, also auf die Platine montiert werden. Das
ist meistens der bessere Weg da hier die Komponenten stabiler montiert werden können. Die Alternative hierzu ist [Plate
mount](#plate-mount).

## <a name="plate">Plate</a>
Die **Plate** ist in manches Keyboards eine Platte oberhalb des [PCB](#pcb) in die alle Switches eingeclippt werden.
Hierdurch erhält man zusätzliche Stabilität und je nach Material der Plate (gängig hier sind bspw. Aluminium, Messing
oder Polycarbonat) auch ein anderes Soundprofil.

## <a name="plate-mount">Plate mount</a>
Bezeichnet das Verfahren wenn Switches und/oder Stabilizer in der [Plate](#plate) montiert werden. Die Alternative hierzu
wäre [PCB Mount](#pcb-mount).

## <a name="profil">Profil</a>
Ein **Profil** bezeichnet die Form einer [Keycap](#keycap). Gängige Profile sind Cherry, OEM, SA, XDA, DSA und DSS.

## <a name="proxy">Proxy</a>
Ein **Proxy** ist ein oftmals geografisch zugeordneter Wiederverkäufer, der sich um Import und Weiterversand von
Produkten kümmert.  Proxies kommen oft während [GBs](#groupbuy) zum Einsatz. Viele davon sind [Vendors](#vendor).

## <a name="groessen">Prozentuale Größen</a>
Oft liest man in der Szene von 40%, 65%, TKL und 100%. Diese Angaben beziehen sich auf die Größe des Keyboards bzw.
wieviele Tasten es besitzt. Ein "normales" Keyboard wie die meisten es kennen wäre **100%** und enthält je nach Layout
meist 104 Tasten. Lässt man den Nummernblock weg, hat man das Format **TKL** oder **85%** (= Tenkeyless = Ohne
Zehnerblock) mit meist 87 Tasten. Lässt man die Anzahl der Tasten nun fast gleich und schiebt einfach nur alles
kompakter zusammen landet man beim *75%* Layout.  Sowohl Fullsize/100% als auch TKL sind noch gängige Formate und man
bekommt es von vielen Herstellern angeboten. Wenn es kleiner wird, wandert man immer weiter in Nerdecke und wird
nichtsmehr von Logitech oder Razor finden weil einfach die große Masse mit solch kleinen Keyboards nicht klarkommt und
daher der Massenmarkt fehlt.  Der nächste Schritt wäre die F-Tasten (Meist F1 bis F12) wegzulassen. Auch hierrauf können
noch die meisten verzichten und man landet beim **65%** Layout. Nächste Verschlankung wäre rechts die Pfeiltasten und
den Block mit Home/Ende/Pos1/etc wegzulassen. Ohne diese Tasten landet man beim **60%** Layout mit meist 61 Tasten.
Jetzt hat man nurnoch Buchstaben, [Modifier](#modifier) und Zahlen. Das kleinste noch gängige Format nennt sich **40%**
und streicht dann auchnoch die Zahlenreihe. Jetzt hat man wirklich nurnoch die Buchstaben und die meisten Modifier, was
manchen tatsächlich als [Daily Driver](#dailydriver) ausreicht. Alles darunter sind meist nurnoch Spaßlayouts (30%) oder
eigentlich nurnoch ein [Macropad](#macropad)

## <a name="rubberdome">Rubberdome</a>
Im gegensatz zu mechanischen Tastaturen arbeiten die meisten gängigen Tastaturen die man so kennt und die die meisten
von euch täglich benutzen mit der **Rubberdome** Technik. Hierbei befindet sich im Switch keine [Metallfeder](#spring)
sondern eine kleine Kuppel aus Gummi (daher der Name **Rubberdome**) die den Widerstand beim  Tippen erzeugt. Diese
Gummikuppel berührt dann beim Herrunterdrücken die Platine und löst so den eigentlichen Tastendruck aus. Der Grund wieso
die meisten Menschen zu mechanischen Tastaturen  wechseln ist dass diese Gummikuppel ein sehr schawmmiges Tippgefühl
erzeugt und keinen sauberen Druckpunkt hat der erkennen lässt dass die Taste gedrückt wurde.

## <a name="split-keyboard">Split Keyboard</a>
Ein **Split Keybord** ist eine Tastatur, welche in mehrere Teile geteilt ist.  Oftmals gibt es eine linke und eine
rechte Seite, für jede Hand eine.

Ein großer Fokus bei **Split Keyboards** ist Ergonomie.  Daher verwenden viele dieser Tastaturen
[ortholineare](#ortholinear-layout) [Layouts](#layout).

## <a name="spring">Spring</a>
Eine **Spring** ist eine kleine Metallfeder wie man sie auch aus Kugelschreibern kennt und  drückt von unten gegen den
[Stem](#stem). Die Spring ist dafür verantwortlich wieviel [Auslösekraft](#auslösekraft) ein Switch hat.

## <a name="stabilizer">Stabilizer</a>
Für alle Tasten mit einer Breite von 2[U](#u-sice) oder mehr benötigt man sogenannte **Stabilizer**. Diese dienen dazu
den [Keycap](#keycap) zu stabilisieren damit er nicht kippt, wenn man ihn am Rand tippt. Stabilizer bestehen aus zwei
kleinen Schlitten die an der Oberseite einen [Stem](#stem) haben und auf der Unterseite durch einen langen Draht
miteinander verbunden werden. Dadurch hebt und senkt sich der Switch gleichmäßig über die volle Länge. Genau wie einen
Switch kann man auch die Stabilizer [luben](#lubing) um Reibung zu verringern und den Sound zu verbessern.


## <a name="staggered-layout">Staggered Layout</a>
Bei einem **staggered Layout** werden die Reihen versetzt (englisch: _staggered_) zueinander angeordnet.  Dies ist das
gängiste Tastaturlayout, welches die meisten Menschen kennen.

Das Gegenteil dieses Layouts ist das [ortholineare Layout](#ortholinear-layout).

## <a name="stem">Stem</a>
Der Stem ist der bewegliche Teil des [Switches](#switch) der oben herrausschaut und auf den der Keycap gesteckt wird. 

## <a name="switch">Switch</a>
Ein **Switches** ist das Bauteil, welches vom Anwender gedrückt werden kann, um einen Tastendruck auszulösen. Er besteht
aus einem [Housing](#housing) welches die [Spring](#spring) und den [Stem](#stem) enthalten. Auf dem Stem steckt in der
Regel eine [Keycap](#keycap).

## <a name="films">Switch Films</a>
**Switch Films** sind ein kleines, sehr dünnes Stück Plastik mit einer Dicke von meist 0,1 oder 0,15mm welches zwischen
Top und Bottom [Housing](#housing) montiert wird um kleine Tolleranzen bei der Produktion auszugleichen. Dadurch sitzen
Top und Bottom stabiler zusammen und man hat weniger Spiel im [Switch](#switch) was sich auf Sound und Tippgefühl
auswirken kann.

## <a name="tactile-switch">Taktile Switches</a>
Bei **taktilen Switches** handelt es sich um eine der drei Grundarten von Switches (neben [linear](#linear-switch) und
[clicky](#clicky-switch)). Bei taktilen Switchen hat man ungefähr in der Mitte des Tastendrucks einen spürbaren
Widerstand den man mit ein bisschen mehr Kraft überwinden muss bevor der Switch auslöst. Daher werden  hier meist auch
zwei Gewichte angegeben: Die *actuationforce* die man benötigt um den Bump zu überwinden sowie die *bottom out force*
die man danach noch braucht um den Switch komplett durchzudrücken.

## <a name="topre">Topre</a>
**Topre** Switches sind ein Produkt der gleichnamigen japanischen Firma welche seit 1983 Keyboards baut. Im ersten
Moment ähnelt ein Topre Switch dem Aufbau von [Rubberdome](#rubberdome) Tasten, hat aber jedoch ein paar kleine
Unterschiede und auch ein vollkommen anderes Schreibgefühl. Unter der Gummikappe befindet sich bei Topre Schaltern
zusätzlich noch eine [Feder](#springg) und die Schalter arbeiten im Gegensatz zu klassischen Rubberdome nicht über den
direkten Berührungskontakt auf der Platine, sondern elektrostatisch kapazitiv.  Ob das jetzt besser oder schlechter ist,
ist ein Streitthema in der Szene. Für manche ist das nur ein überteuerter Rubberdome Switch, für andere das beste was
man aktuell kaufen kann.

## <a name="u-size">U Maßeinheit</a>
Die Breit von Tasten wird in **U** angegeben. Die normalen Buchstaben- und Zahlentasten haben eine Breite von 1U. Alle
anderen Tasten auf dem Keyboard sind dann in Relation dazu ein Vielfaches in der Breite was jedoch sehr stark vom
[Layout](#layout) abhängig ist. In einem normalen deutschen [ISO](#ISO) Layout findet man beispielsweise folgende Größen:

* 1,25U: Linkes Shift, Strg, Alt, Windows, AltGr
* 1,5U: Tab
* 1,75U: CapsLock
* 2U: Backspace
* 2,75U: Rechtes Shift
* 6,25U: Leertaste

## <a name="vendor">Vendor</a>
Ein **Vendor** ist ein Verkäufer, und betreibt in der Regel einen Shop für Endkunden.
