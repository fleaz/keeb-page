<!--
.. title: Herzlich willkommen!
.. slug: index
.. date: 2021-10-13 21:50:05 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Willkommen auf _Keebs, Keebs, Keebs!_, deiner Seite für Informationen zu mechanischen Tastaturen in deutscher Sprache.

Du findest hier eine Übersicht über [gängige Begriffe](/glossar/) des Hobbies, wichtige [Communities](/communities/), [Medien](/medien/) und mehr.

Falls du diese Seite auf einem Mobilgerät anschaust, findest du das Menü hinter dem Kreis am unteren Ende der Seite.
