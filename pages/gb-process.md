<!--
.. title: Ablauf eines Group Buys
.. slug: gb-process
.. date: 2021-11-27 20:25:14 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

In der überschaubaren Tastatur-Szene werden viele Produkte nur über _Group Buys_ (kurz _GB_) verkauft. Dabei werden die Herstellungskosten durch verschiedene Interessierte im Voraus bezahlt, was die Herstellung auch in kleinen Stückzahlen ermöglicht.

Gleich vorne weg: Die Beteiligung an einem Group Buy ist dabei kein Garant dafür ein fertiges Produkt zu erhalten.
Unterschiedlichste Umstände können dazu führen, dass nur Teile realisiert oder vielleicht auch das komplette Projekt nicht durchgeführt werden kann.
Ob man das bezahlte Geld dabei zurück erstattet bekommt, hängt von den jeweiligen Umständen ab und lässt sich nicht pauschal sagen.

Ungeachtet der Umstände gibt es in der Regel die folgenden Phasen:

* Interest Check
* Group Buy
* Manufacturing
* Delivery


## Interest Check

Der erste Schritt ist in der Regel ein _Interest Check_, kurz _IC_. Der IC dient für den Initiator dazu das Interesse am Produkt zu überprüfen, zu sehen ob genug Personen das fertige Produkt kaufen würden.
Ideen ohne genügend Zuspruch werden oftmals nicht weiter verfolgt und das Produkt wird nicht Realität.

Der IC ist der ideale Zeitpunkt für Feedback zu den ursprünglichen Ideen. Oftmals sind noch nicht alle Parameter festgelegt, Eigenschaften wie bspw. Material oder Farben können angepasst werden.

Viele ICs haben dedizierte Feedback-Formulare, um gezielte Rückmeldungen zu erfragen.

Während dieser Phase werden oftmals auch Musterstücke erstellt, um einen späteren Erfolg sicherzustellen.
Es finden Verhandlungen mit Herstellern statt, in deren Verlauf über Preise oder Mindestmengen

Nicht jeder Group Buy hat einen IC, manche GBs haben mehrere ICs.


## Group Buy

In dieser Phase gibt es die Möglichkeit das Produkt zu kaufen.

Diese Phase ist meistens zeitlich begrenzt, manchmal gibt es auch eine Begrenzung der verfügbaren Produkte oder beides gleichzeitig.

Es muss oftmals eine Mindestmenge, MOQ genannt, erreicht werden, bevor ein Produkt tatsächlich produziert wird, da die Produktion ansonsten nicht wirtschaftlich ist.

Manchmal werden Produkte über geografisch verteilte _Proxys_ verkauft. Diese kümmern sich um den Import und den Weiterversand.
Für Käufer ist dies oftmals billiger und weniger aufwändig als diese Schritte selbst zu übernehmen.

Viele _GBs_ werden heutzutage über reguläre Online-Shops verkauft und ein Einkauf gestaltet sich wie reguläres Online-Shopping, aber mit deutlich längerer Wartezeit.

Ist der Verkauf beendet, wird eine Bestellung an den vorher angefragten Hersteller ausgelöst.
Durch die Bestellungen kann die benötigte Menge sehr einfach abgeleitet werden.


## Manufactoring

Die Bestellung wird durchgeführt und das Produkt hergestellt.

Käufer warten während dieser Zeit darauf, dass das Produkt hergestellt wird.

In manchen Fällen gehen dieser Phase noch Abstimmungen zwischen Initiator und Auftragsfertiger voraus, so wird bspw. bei Keycaps oftmals ein _Colormatching_ durchgeführt, um die erwünschten Farben zu erzielen.

Oftmals wird während der Herstellung eine Qualitätskontrolle durch den Hersteller durchgeführt, manchmal wird diese auch vom Initiator nach Anlieferung der Produkte durchgeführt.


## Delivery

Das fertige Produkt wird vom Hersteller an den Initiator des Group Buys versendet. Dieser versendet das Produkt gegebenfalls an die beteiligten Proxies, welche den Weiterversand in ihrer Region übernehmen.

Beim Käufer sollte in dieser Zeit irgendwann ein Paket eintrudeln, welches das lang ersehnte Produkt erhält.


## Extras

Oftmals werden vom Initiator oder den beteiligten Händlern zusätzliche Stücke bestellt.
Oftmals geschieht dies um eventuell defekte Produkte ersetzen zu können oder um eine MOQ zu erreichen.

Nicht mehr benötigte Extras werden oftmals regulär verkauft, aber es ist nicht immer einfach hier den Zuschlag zu bekommen.
Es könnte allerdings eine gute Chance sein doch noch an ein begehrtes Produkt zu kommen.
