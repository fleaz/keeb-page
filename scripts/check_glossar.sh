#! /usr/bin/env bash
# This script will get all heading from the glossar and check if they are correctly sorted

ENTRIES=`cat pages/glossar.md | grep "## <a name" | cut -d">" -f2 | cut -d"<" -f1`
SORTED=`printf "$ENTRIES" | sort`

diff -y <(printf "$ENTRIES") <(printf "$SORTED")

EXIT=$?

if [ $EXIT -eq 0 ] ; then
  echo -e "\n**********\n LGTM! \n**********"
else
  echo -e "\n**********\n The glossar is not correctly sorted! \n**********"
fi

exit $EXIT
